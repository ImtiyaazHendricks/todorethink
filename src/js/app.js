import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './reducers';
import Home from './components/Home';

console.log(Home);

ReactDom.render(
	<Provider store={store}>
    	<Home />
    </Provider>
, document.getElementById("root"));